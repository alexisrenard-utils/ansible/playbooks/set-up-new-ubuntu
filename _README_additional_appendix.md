## Appendix
### Working with git submodules

* Add a submodule
```bash
git submodule add <repo_url>
```

* Update the submodules
```bash
git submodule update --recursive
git submodule foreach --recursive git fetch
git submodule foreach --recursive git checkout master
```

* Remove a gitsubmodule
```bash
rm -rf <path_to_the_module>
# -> Then stage the potential modifications you've done in the .gitmodule file.
git rm -r --cached <path_to_the_module>
# -> Then open and remove submodule entry in .gitmodules file.
```

*git version 2.17.1*
